<?php

class User {

    private $errors = array();

    public $password;
    public $cpassword;
    public $hashed_password;

    public function signup($POST) {
        // validate
        foreach ($POST as $key => $value) {
            // username

            if($key == "username") {

                if(trim($value) == "") {
                    $this->errors[] = "Please enter a valid username";
                }

                if(is_numeric($value)) {
                    $this->errors[] = "Username could not be a number";
                }

                if(preg_match("/[0-9]+/", $value)) {
                    $this->errors[] = "Username cannot contain numbers";
                }
            }

            // email
            if($key == "email") {
                if(trim($value) == "") {
                    $this->errors[] = "Please enter a valid email";
                }

                if(!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                    $this->errors[] = "Email is not valid";
                }

            }   


            if($key == "password") {
                $this->password = $value;

                if(trim($value) == '') {
                    $this->errors[] = "Please enter a valid password";
                }

                if(strlen($value) < 6) {
                    $this->errors[] = "Password must be atleast 6 characters";
                }
            }   

            // confirme password
            if($key == "cpassword") {
                $this->cpassword = $value;

                if($this->password != $this->cpassword) {
                    $this->errors[] = "Passwords must be the same";
                }

                if(trim($value) == '') {
                    $this->errors[] = "Please enter a valid password";
                }

                if(strlen($value) < 6) {
                    $this->errors[] = "Password must be atleast 6 characters";
                }
            }

        }
        // save to database
        if(count($this->errors) == 0) {
            $this->hashed_password = password_hash($this->password, PASSWORD_DEFAULT);

            $query = "INSERT INTO users (username, email, password, date) VALUES (:username, :email, :hashed_password, :date)";
            $DB = new Database();

            $data = array();
            $data["username"] = $POST["username"];
            $data["email"] = $POST["email"];
            $data["hashed_password"] = $POST["password"];
            $data["date"] = date("Y-m-d H:i:s");
            $result = $DB->write($query, $data);

            if(!$result) {
                $this->errors[] = "Your data could not be saved";
            }
        }

        return $this->errors;
    }
}