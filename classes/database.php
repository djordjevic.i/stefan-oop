<?php 

class Database {
    private $HOST = "127.0.0.1";
    private $USER = "stefan";
    private $PASS = "12345678";
    private $DB_NAME = "oopdb";
    public $conn;

    public function __construct()
    {
        $string = "mysql:host=$this->HOST; dbname=$this->DB_NAME";

        try{
            $this->conn = new PDO($string, $this->USER, $this->PASS);

        } catch(PDOException $e) {
            die($e->getMessage());
        }
        return $this->conn;

    }
    
    public function write($query, $data = array())
    {
        $stmt = $this->conn->prepare($query);
        $result = $stmt->execute($data);

        if($result) {
            return true;
        } else {
            return false;
        }
    }
}