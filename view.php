<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Viwe users</title>
</head>
<body>
<?php 
    include_once("classes/show.php"); 
    include_once("classes/delete.php");
    $viewdata= new Show();
    $data = $viewdata->showUser();
    $output = "";
    $output .="<table border= 1;>
            <thead>
                <tr>
                    <th>Id</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>";
            foreach ($data as $dat) {
                $output.="<tr>
                    <td>".$dat['id']."</td>
                    <td>".$dat['username']."</td>
                    <td>".$dat['email']."</td>
                    <td><a href='config/delete_process.php?id=".$dat['id']."'>Delete</a></td>
                </tr>";
            }
    $output .= "</tbody>
    </table>";
    echo $output;
?>
</body>
</html>