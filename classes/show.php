<?php 
include_once('database.php');

class Show  extends Database {

    public $table = "users";

    public function __construct()
    {
        parent::__construct();
    }

    public function showUser() {
        $sqlInsert = "SELECT * FROM $this->table";
        $queryInsert = $this->conn->query($sqlInsert);
        $data = array();

        if($queryInsert->rowCount() > 0) {
            while ($row = $queryInsert->fetch(PDO::FETCH_ASSOC)) {
                $data[] = $row;
            }
            return $data;
        } else {
            return false;
        }
    }
}