<?php 
     $username = "";
     $email = "";
     $password = "";
     $cpassword = "";
 
    if(count($_POST) > 0) {
        require "config/autoload.php";
        $User = new User();
        $errors = $User->signup($_POST);

        if(count($errors) == 0) {
            header("location: index.php");
            die;
        }


        extract($_POST);
    }

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sign up</title>\
    <style>
        body {
            font-family:'Gill Sans', 'Gill Sans MT', Calibri, 'Trebuchet MS', sans-serif;
        }

        form {
            padding: 10px;
            border: solid thin #aaa;
            border-radius: 10px;
            margin: auto;
            width: 500px;
        }

        .textbox {
            padding: 5px;
            border-radius: 5px;
            font-size: 13px;
            width: 95%;
            border: solid thin #aaa;
            margin-top: 10px;
            outline: none;
        }

        .btn {
            border-radius: 5px;
            padding: 10px;
            background-color: black;
            color: white;
            float: right;
            border: none;
            cursor: pointer;
        }

        .btn-2 {
            border-radius: 5px;
            padding: 10px;
            background-color: black;
            color: white;
            float: left;
            border: none;
            cursor: pointer;
            text-decoration: none;
        }


        .error {
            color: white;
            background-color: red;
            padding: 4px;
            text-align: center;
        }
    </style>
</head>
<body>
    <form method="POST">

        <?php if(isset($errors) && is_array($errors) && count($errors) > 0):?>
            <div class="error">
                <?php foreach($errors as $error):?>
                <?=$error?><br>
                <?php endforeach;?>
            </div>
        <?php endif;?> 

        <h2>Sign up</h2>
        <?php

        ?>
        <input class="textbox" type="text" name="username" placeholder="Enter username" value="<?=$username?>"><br>
        <input class="textbox" type="text" name="email" placeholder="Enter email" value="<?=$email?>"><br>
        <input class="textbox" type="password" name="password" placeholder="Enter password"<?=$password?>><br>
        <input class="textbox" type="password" name="cpassword" placeholder="Confirm passsword" value="<?$cpassword?>"><br>
        <br>
        <input class="btn" type="submit" value="Signup">
        <a href="view.php" class="btn-2">View</a>
        <br style="clear: both;">
    </form>
</body>
</html>