<?php 
include_once('database.php');

class Delete extends Database {

    public $table = "users";

    public function __construct()
    {
        parent::__construct();
    }

    public function deleteUser($id) {
        $sqlDel = "DELETE FROM $this->table WHERE id=$id";
        $query = $this->conn->query($sqlDel);
        $data = array();
        if($query) {
            return true;
        } else {
            return false;
        }
    }
}